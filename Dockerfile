FROM nginx:latest
ARG HTTP_PORT=80
ADD default.conf /etc/nginx/conf.d/
ADD index.html /usr/share/nginx/html
# ADD index2.html /usr/share/nginx/html
RUN sed -i 's/{{VAR_HTTP_PORT}}/'${HTTP_PORT}'/g' /etc/nginx/conf.d/default.conf
EXPOSE ${HTTP_PORT}
